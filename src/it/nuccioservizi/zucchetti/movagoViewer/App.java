package it.nuccioservizi.zucchetti.movagoViewer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.eclipse.jdt.annotation.NonNull;

public final class App {

    private static <T> List<T> asUnmodifiableList(final List<T> list) {
        @SuppressWarnings("null")
        @NonNull
        final List<T> u = Collections.unmodifiableList(list);

        return u;
    }

    private static final class Campo {
        final String nome;
        final int inizio;
        final int fine;
        final String note;

        Campo(final String nome, final int inizio, final int fine,
                final String note) {
            this.nome = nome;
            this.inizio = inizio;
            this.fine = fine;
            this.note = note;
        }

        @Override
        public String toString() {
            return "Campo [nome=" + this.nome + ", inizio=" + this.inizio
                    + ", fine=" + this.fine + ", note=" + this.note + "]";
        }
    }

    private static final class MetaMovago {
        final List<TipoRecord> tipiRecord;
        final int recordSize;

        MetaMovago(final List<TipoRecord> tipiRecord, final int recordSize) {
            this.tipiRecord = tipiRecord;
            this.recordSize = recordSize;
        }
    }

    private static <T> T ultimo(final List<T> l) {
        return l.get(l.size() - 1);
    }

    private static int sizeTipoRecord(final List<Campo> campi) {
        return ultimo(campi).fine;
    }

    private static final class TipoRecord {
        /** Identificativo per il tipo record. */
        final String codTipo;
        final List<Campo> campi;

        TipoRecord(final String codTipo, final List<Campo> campi) {
            this.codTipo = codTipo;
            this.campi = campi;
        }

        boolean dataIsRecordOfThisType(final String data) {
            return data.startsWith(this.codTipo);
        }

        public void print(final String record) {
            for (final Campo campo : this.campi) {
                System.out.println(String.format("%s=%s", campo.nome,
                        record.substring(campo.inizio - 1, campo.fine)));
            }
            System.out.println();
        }

        @Override
        public String toString() {
            return "TipoRecord [codTipo=" + this.codTipo + ", campi="
                    + this.campi + "]";
        }
    }

    public static void addTipoRecord(final List<TipoRecord> tipiRecord,
            final String codTipoRecord, final List<Campo> campi,
            final int recordSize) {
        if (codTipoRecord.isEmpty() && !campi.isEmpty())
            throw new IllegalStateException(
                    "Campi senza codice tipo record: " + campi);

        final int thisSize = sizeTipoRecord(campi);
        if (recordSize != thisSize)
            throw new IllegalStateException(String.format(
                    "Il tipo record %s ha dimensione %s, ma dovrebbe essere di %s caratteri.",
                    codTipoRecord, String.valueOf(thisSize),
                    String.valueOf(recordSize)));

        tipiRecord.add(new TipoRecord(codTipoRecord, campi));
    }

    private static Path getPath(final String fileName) {
        final Path p = Paths.get(fileName);
        if (p == null)
            throw new IllegalStateException("Path null per: " + fileName);

        return p;
    }

    private static BufferedReader newBufferedReader(final Path path)
            throws IOException {
        final BufferedReader r = Files.newBufferedReader(path);
        if (r == null)
            throw new IllegalStateException("BufferedReader null per: " + path);

        return r;
    }

    public static void main(final String[] args) throws Exception {
        final Path movagoRecordsPath = getPath("MOVAGO-records300.csv");
        final Path fileDaVerificare = getPath("MOVAGO.txt");

        final MetaMovago metaMovago = readMetaMovago(movagoRecordsPath);

        printRecords(fileDaVerificare, metaMovago);
    }

    private static MetaMovago readMetaMovago(final Path movagoRecordsPath)
            throws IOException {
        /*
         * Ogni tipo record ha una testata che serve per:
         *
         * - ottenere il codice che identifica il record (i primi caratteri)
         *
         * - sapere dove comincia la tabella che descrive i campi del record
         */
        final Pattern patternIdFormato = Pattern.compile(
                "\\AID-Formato: CodTipoRec=.(.).(?: CodTipoDett=(\\S+))?.*");

        final List<TipoRecord> tipiRecord = new ArrayList<>();
        int recordSize = 0;

        try (final Reader movagoRecordsReader = newBufferedReader(
                movagoRecordsPath);
                final CSVParser parser = CSVFormat.DEFAULT
                        .parse(movagoRecordsReader)) {

            boolean nextIsHeader = false;
            String codTipoRecord = "";
            List<Campo> campi = new ArrayList<>();

            for (final CSVRecord record : parser) {
                if (nextIsHeader) {
                    nextIsHeader = false;
                    // Non ci interessa la riga con le intestazioni
                    continue;
                }

                final String nome = record.get(0);
                if (nome == null)
                    throw new IllegalStateException("Manca il primo campo");

                if (nome.isEmpty())
                    // Record vuoto: passiamo al prossimo
                    continue;

                final Matcher matcherIdFormato = patternIdFormato.matcher(nome);
                if (matcherIdFormato.matches()) {
                    // La riga dopo è l'header della tabella campi
                    nextIsHeader = true;

                    if (!campi.isEmpty()) {
                        // Abbiamo letto una tabella campi

                        if (recordSize == 0)
                            // È il primo tipo record: ricordiamo la dimensione
                            recordSize = sizeTipoRecord(campi);

                        addTipoRecord(tipiRecord, codTipoRecord, campi,
                                recordSize);

                        campi = new ArrayList<>();
                    }

                    codTipoRecord = matcherIdFormato.group(1) + Optional
                            .ofNullable(matcherIdFormato.group(2)).orElse("");

                    // L'intestazione non contiene altri dati
                    continue;
                }

                final int inizio = getInt(record, 2);
                final int fine = getInt(record, 3);
                final String note = getStringIfExists(record, 5);

                campi.add(new Campo(nome, inizio, fine, note));
            }

            addTipoRecord(tipiRecord, codTipoRecord, asUnmodifiableList(campi),
                    recordSize);
        }

        return new MetaMovago(asUnmodifiableList(tipiRecord), recordSize);
    }

    private static void printRecords(final Path fileDaVerificare,
            final MetaMovago metaMovago) throws IOException {

        try (final Reader in = newBufferedReader(fileDaVerificare)) {

            // Possiamo riusare buf perché viene copiato in una String.
            final char[] buf = new char[metaMovago.recordSize];

            do {
                boolean trovatoQualcosa = false;

                final String record = readRecord(in, buf);

                if (record.isEmpty())
                    break;

                for (final TipoRecord tipoRecord : metaMovago.tipiRecord)
                    if (tipoRecord.dataIsRecordOfThisType(record)) {
                        tipoRecord.print(record);
                        trovatoQualcosa = true;
                    }

                if (!trovatoQualcosa)
                    System.err.println("Record di tipo sconosciuto: " + record);

            } while (true);
        }
    }

    private static int getInt(final CSVRecord record, final int index) {
        return Integer.parseInt(record.get(index));
    }

    private static String getStringIfExists(final CSVRecord record,
            final int index) {
        @SuppressWarnings("null")
        @NonNull
        final String s = record.size() < index + 1 ? "" : record.get(index);

        return s;
    }

    /**
     * Legge buf.length caratteri da in. Se ne sono disponibili di meno, solleva
     * un'eccezione {@link IllegalStateException}.
     *
     * @param in  Da dove leggere i caratteri.
     * @param buf Buffer di appoggio che fornisce anche il numero di caratteri
     *            da leggere.
     * @return Una stringa vuota se non ci sono più caratteri da leggere. Una
     *         stringa di buf.length caratteri letti da in.
     * @throws IOException           Per errori di IO su in.
     * @throws IllegalStateException Se non si riescono a leggere buf.length
     *                               caratteri pur non essendo esaurito l'input.
     */
    private static String readRecord(final Reader in, final char[] buf)
            throws IOException {
        final int count = in.read(buf, 0, buf.length);

        if (count == -1)
            // Non ci sono più record
            return "";

        if (count != buf.length)
            // Il record letto è incompleto
            throw new IllegalStateException(
                    "Richiesti " + buf.length + ", letti " + count);

        @SuppressWarnings("null")
        @NonNull
        final String record = String.valueOf(buf);

        return record;
    }
}
