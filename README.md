# movagoViewer

Per importare i dati della contabilità in Zucchetti AGO, bisogna creare un file
con record a larghezza fissa chiamato MOVAGO. Trovare errori in questo file non
è semplice perché i record non sono separati da un fine linea e i singoli
record sono lunghi centinaia di caratteri senza alcun separatore tra i campi.

Per questo motivo ho creato questo programmino che legge un file MOVAGO e scrive
il nome dei campi e i valori letti in un formato facilmente leggibile.

Di MOVAGO esistono due versioni, una con record di 300 caratteri e l'altra con
record di 500 caratteri. Il formato dei record, cioè nome, posizione e lunghezza
dei campi, è documentato in un file PDF. Non è facile estrarre le informazioni
dal file PDF, per questo l'ho convertito in formato CSV e uso questo come input
per dedurre la struttura del file da leggere.

Quando Zucchetti farà un aggiornamento al formato dei record, sarà sufficiente
fare un nuovo file CSV per leggere i file creati col nuovo formato.

*N.B.*: per ora il programma non è interattivo ed è necessario ricompilarlo per
modificare il nome dei file da leggere, sia per il formato, sia per i dati.
Questa operazione è abbastanza semplice usando Maven o un qualsiasi IDE Java
come Eclipse.